using Xunit;

namespace CoverletDemo.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var service = new Service();
            
            //comment out next line to get some coverage
            service.StartMassTransit(); 

            service.Foo(true);
            service.Foo(false);
        }
    }
}
