﻿using MassTransit;
using System;

namespace CoverletDemo
{
    public class Service
    {
        public void Foo(bool a)
        {
            if (a)
                Console.Write("1");
            else
                Console.Write("0");
        }

        public void StartMassTransit()
        {
            var busControl= Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://localhost"), h => { });

                cfg.ReceiveEndpoint(host, "event_queue", e =>
                {
                    e.Handler<SomeMessage>(context => Console.Out.WriteLineAsync("Message received"));
                });
            });

            busControl.Start();
        }
    }

    class SomeMessage
    {
        public int MyProperty { get; set; }
    }
}
